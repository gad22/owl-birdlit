<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet  version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--Output raw xml from a database retrieval-->
   <xsl:output method="xml" encoding="ISO-8859-1" indent="yes"/>
<xsl:strip-space elements="*"/>
<xsl:template match="/">
<xsl:apply-templates/>  
</xsl:template>
<xsl:template match="root">
<xsl:message>
      got root
</xsl:message>
<xsl:element name="recordset">
<xsl:for-each  select="*">
<!--tagname above depends on table name -->
<xsl:copy-of select="."/>
<xsl:text>&#10;</xsl:text>
</xsl:for-each>
</xsl:element>
 </xsl:template>
  </xsl:stylesheet>	
  