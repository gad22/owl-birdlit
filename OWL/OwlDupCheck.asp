<%@ Language=VBScript %>
<%

	'** ANTI CACHING CODE **
	Response.Expires = 60
	Response.Expiresabsolute = Now() - 1
	Response.AddHeader "pragma","no-cache"
	Response.AddHeader "cache-control","private"
	Response.CacheControl = "no-cache"

	dupflds = ""

	if request.form("author") = "on" then
		if len(dupflds) > 0 then
			dupflds = dupflds & " + "
		end if
		dupflds = dupflds & "author"
	end if

	if request.form("authorkey") = "on" then
		if len(dupflds) > 0 then
			dupflds = dupflds & " + "
		end if
		dupflds = dupflds & "authorkey"
	end if

	if request.form("year") = "on" then
		if len(dupflds) > 0 then
			dupflds = dupflds & " + "
		end if
		dupflds = dupflds & "[year]"
	end if

	if request.form("title") = "on" then
		if len(dupflds) > 0 then
			dupflds = dupflds & " + "
		end if
		dupflds = dupflds & "title"
	end if

	if request.form("serial") = "on" then
		if len(dupflds) > 0 then
			dupflds = dupflds & " + "
		end if
		dupflds = dupflds & "serial"
	end if

	if request.form("volume") = "on" then
		if len(dupflds) > 0 then
			dupflds = dupflds & " + "
		end if
		dupflds = dupflds & "volume"
	end if

	if request.form("spage") = "on" then
		if len(dupflds) > 0 then
			dupflds = dupflds & " + "
		end if
		dupflds = dupflds & "spage"
	end if

	if request.form("fpage") = "on" then
		if len(dupflds) > 0 then
			dupflds = dupflds & " + "
		end if
		dupflds = dupflds & "fpage"
	end if

	if request.form("abstractor") = "on" then
		if len(dupflds) > 0 then
			dupflds = dupflds & " + "
		end if
		dupflds = dupflds & "abstractor"
	end if

	if request.form("source") = "on" then
		if len(dupflds) > 0 then
			dupflds = dupflds & " + "
		end if
		dupflds = dupflds & "source"
	end if

		
	set con = server.CreateObject("ADODB.Connection")
	con.Provider = "sqloledb"
	con.Properties("Network Library") = "DBMSSOCN"
	con.Properties("Data Source") = "esql2k501.discountasp.net"
	con.Properties("Initial Catalog") = "SQL2005_544397_owl"
	con.Properties("User ID") = "SQL2005_544397_owl_user"
	con.Properties("Password") = "roltits"
	con.Open

	SQLstr = "select * from owlmaster where (" & dupflds & ") in(select (" & dupflds & ") from owlmaster " 
	SQLstr = SQLstr & "group by " & dupflds
	SQLstr = SQLstr & " having count(recordid) > 1) "
	SQLstr = SQLstr & "order by " & dupflds

	set r = con.Execute(SQLstr)

	Dim FldArr(16)
	FldArr(0) = "recordid"
	FldArr(1) = "source"
	FldArr(2) = "title"
	FldArr(3) = "author"
	FldArr(4) = "authorkey"
	FldArr(5) = "serial"
	FldArr(6) = "address"
	FldArr(7) = "email"
	FldArr(8) = "abstractor"
	FldArr(9) = "keywords"
	FldArr(10) = "year"
	FldArr(11) = "volume"
	FldArr(12) = "spage"
	FldArr(13) = "fpage"
	FldArr(14) = "abstract"
	FldArr(15) = "inserted"
	FldArr(16) = "recordstatus"


	function FixTabFormat(strIn)
		newStr = Replace(strIn,chr(9)," ")
		newStr = Replace(newStr,chr(10),"")
		newStr = Replace(newStr,chr(13),"")
		FixTabFormat = newStr
	end function
%>

<html>
<head>

<style type="text/css">
	table.outtbl td
	{
		border: solid 1px black;
		padding:2px 4px 2px 2px;
		font:8pt arial;
	}
	.Results
	{
		font: 10pt arial black;
		color:maroon;
	}

</style>

</head>
<body>
<%

	if Request.Form("OutputType") = "HTML" then
		response.write("<span class=Results>Results for duplicate checks on: " & Replace(dupflds,"=on","") & "</span><br/>")
		response.write("<br/>")
		Response.Write("<table id='outtbl' class='outtbl' cellspacing=0 cellpadding=0>")
		For Each item In FldArr
	   	Response.Write("<td>" & item & "</td>")
		Next
	
		while not r.eof
		   Response.Write("<tr valign='top'>")
		   For Each item In FldArr
			if IsNull(r(item)) then
				Response.Write("<td>&nbsp;</td>")
			else
				val = trim(r(item))
				if len(val) = 0 then
				   Response.Write("<td>&nbsp;</td>")
				else
				   Response.Write("<td>" & val & "</td>")
				end if
			end if

		   next
		   Response.Write("</tr>")
		   r.movenext
		wend

		Response.Write("</table>")
	end if

	if Request.Form("OutputType") = "Tab" then
		Response.Buffer = true
		Response.Clear()
		Response.ContentType = "text"
		Response.AddHeader "Content-Disposition","attachment; filename=OWLDupCheck.txt"
		
		response.write("---------------- REMOVE HEADER ----------------------------" & chr(10))
		response.write("Results for duplicate checks on: " & Replace(dupflds,"=on","") & chr(10))
		response.write("--------------- HEADER END -----------------------------" & chr(10))
	
		FirsTFld = true
		For Each item In FldArr
	   	   if not FirsTFld then
			Response.Write(chr(9))
		   end if
	  	   Response.Write(item)
		   FirstFld = false
		Next
		Response.Write(chr(10))

		while not r.eof
		   FirstFld = true
		   For Each item In FldArr
			if not FirsTFld then
				Response.Write(chr(9))
		   	end if
			if not IsNull(r(item)) then
				val = trim(r(item))
				if len(val) > 0 then
				   Response.Write(FixTabFormat(val))
				end if
			end if
			FirstFld = false
		   next
		   Response.Write(chr(10))
		   r.movenext
		wend
	end if

	con.close
	set con = nothing

%>

