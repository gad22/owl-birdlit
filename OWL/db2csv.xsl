<xsl:stylesheet  version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><!--Output csv from a database retrieval-->
<xsl:output method="text" encoding="ISO-8859-1"/>
<xsl:strip-space elements="*"/>
<xsl:template match="/">
<xsl:apply-templates/>  
</xsl:template>
<xsl:template match="root">
<xsl:message>
      got root
</xsl:message>
<!-- must define record headings here in correct order!!-->
<xsl:text>Query:&#9;</xsl:text><xsl:value-of
select="//query"/><xsl:text>
</xsl:text>
<xsl:text>recordid&#9;authorkey&#9;author&#9;year&#9;title&#9;serial&#9;volume&#9;spage&#9;fpage&#9;address&#9;abstract&#9;abstractor&#9;keywords&#9;source&#9;inserted&#9;oktoshow&#9;recordstatus
</xsl:text>
<!-- imported text here -->
<xsl:for-each  select="dbo.OWLmaster">
<!--tagname above depends on table name -->
<xsl:value-of select="recordid"/><xsl:text>&#9;</xsl:text>
<xsl:value-of select="normalize-space(authorkey)"/><xsl:text>&#9;</xsl:text>
<xsl:value-of select='normalize-space(author)'/><xsl:text>&#9;</xsl:text>
<xsl:value-of select="year"/><xsl:text>&#9;</xsl:text>
<xsl:value-of select='normalize-space(title)'/><xsl:text>&#9;</xsl:text>
<xsl:value-of select="normalize-space(serial)"/><xsl:text>&#9;</xsl:text>
<xsl:value-of select="volume"/><xsl:text>&#9;</xsl:text>
<xsl:value-of select="spage"/><xsl:text>&#9;</xsl:text>
<xsl:value-of select="fpage"/><xsl:text>&#9;</xsl:text>
<xsl:value-of select="normalize-space(address)"/><xsl:text>&#9;</xsl:text>
<xsl:value-of select="normalize-space(abstract)"/><xsl:text>&#9;</xsl:text>
<xsl:value-of select="abstractor"/><xsl:text>&#9;</xsl:text>
<xsl:value-of select="normalize-space(keywords)"/><xsl:text>&#9;</xsl:text>
<xsl:value-of select="normalize-space(source)"/><xsl:text>&#9;</xsl:text>
<xsl:value-of select="inserted"/><xsl:text>&#9;</xsl:text>
<xsl:value-of select="oktoshow"/><xsl:text>&#9;</xsl:text>
<xsl:value-of select="recordstatus"/><xsl:text>&#9;</xsl:text>
<xsl:text>
</xsl:text>
</xsl:for-each>
 </xsl:template>
  </xsl:stylesheet>	
  