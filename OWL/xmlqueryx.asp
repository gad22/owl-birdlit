<%
private x
Function checkfield(fname,ftype)
'fname is field name, ftype search type
'ftype=0 generates LIKE syntax
'ftype=1 generates CONTAINS(* syntax
'ftype=2 generates CONTAINS(fname, symtax
'ftype=3 generates = syntax
'ftype=4 allows operators and between syntax
'ftype=5 deals with dates
param = request(fname)
if x > 0 then
xfilter = " AND "
else
xfilter = " WHERE "
end if
if param <> "" then
select case ftype
case 0
checkfield = xfilter & fname & " like '%" & param & "%'"
case 1 
checkfield = xfilter & " CONTAINS(*,'" & param & "') "
case 2
checkfield = xfilter & " CONTAINS(" & fname & ",'" & param & "') "
case 3
checkfield = xfilter & fname & " = '" & param & "'"
case 4 
checkfield = xfilter & fname & " " & param
case 5
checkfield = xfilter & fname & " " & fixdate(param)
end select
x = 1
else checkfield = ""
end if
end function
Function fixdate(rdate)
'does type conversion for dates, preserving operator if there is one
if left(rdate,1)="<" or left(rdate,1)=">" or left(rdate,1)="=" then
oper = left(rdate,1)
stdate = right(rdate,len(rdate)-1)
else 
oper = ""
stdate = rdate
end if
fixdate = oper & "convert(datetime,'" & stdate &"',103)"
end function
Dim objConn, objRS, strQuery
Dim strConnection, blnRetrieveData
'response.write "<br>Entering xmlquery <br/>"
Set objConn = Server.CreateObject("ADODB.Connection")
objConn.Open "dsn=OWLaccess;database=ROL;UID=roluser;PWD=roltits"
'blnRetrieveData = False
'get data from the query form
format = request("format")
tabname = request("dbtable")
'response.Write "requested table is:" + tabname
'tabname = "dbo.OWLmaster"
qn = "select * from " & tabname
qn = qn & checkfield("rolcode",0)
qn = qn & checkfield("serial",0)
qn = qn & checkfield("volume",3)
qn = qn & checkfield("spage",3)
qn = qn & checkfield("authorkey",0) 
qn = qn & checkfield("year",0)
qn = qn & checkfield("dog",1)
qn = qn & checkfield("title",2) 
qn = qn & checkfield("abstract",2) 
qn = qn & checkfield("keywords",2)
qn = qn & checkfield("recordstatus",4)
qn = qn & checkfield("inserted",5)
qn = qn & " order by authorkey, year desc "
strQuery = qn & " for xml auto, elements"
response.write "<br/>" + strQuery + "<br/>"
sedinf = request.Form("edinf")
'response.write "value of sedinf is:" + sedinf
sincabs = request.Form("incabs")
'response.write " value of incabs is:" + sincabs
Set objRS = objConn.Execute(strQuery)
if not objRS.EOF then
dim  regEx 
result = objRS.GetString
'for debugging - server must be running VBscript 5.0 or higher
'response.write "VBscript " & ScriptEngine & "." & ScriptEngineMajorVersion & "." & ScriptEngineMinorVersion & "<br/>"
'following palaver needed because ODBC shoves in random \r at end of each 4k block!!
set regEx = new regexp
regEx.Global=true
regEx.Pattern="\r"
while regEx.Test(result)
'only attempt replacement if \r found should loop only once
'response.write "Found return in result string"
result = regEx.Replace(result,"")
wend

Dim xmldoc 
Dim xsldoc

'Use the MSXML 4.0 Version dependent PROGID 
'MSXML2.DOMDocument.4.0 if you wish to create
'an instance of the MSXML 4.0 DOMDocument object
 
Set xmldoc = Server.CreateObject("MSXML2.DOMDocument.4.0") 
Set xsldoc = Server.CreateObject("MSXML2.DOMDocument.4.0")
xmlhdr = "<?xml version=""1.0"" encoding=""ISO-8859-1""?><root managestuff='"
xmlhdr = xmlhdr & sedinf & "' abstract='" & sincabs & "'>"
result = xmlhdr & result & "</root>"
if format="xml" then
'Response.Write "<br/>Raw XML----------------------------" & "<BR>&#10;"
Response.Write result
else
xmldoc.loadXML( result)	

'Check for a successful load of the XML Document.
if xmldoc.parseerror.errorcode <> 0 then 
  Response.Write "Error loading XML Document :" & "<BR>"
  Response.Write "----------------------------" & "<BR>"
  Response.Write "Error Code : " & xmldoc.parseerror.errorcode & "<BR>"
  Response.Write "Reason : " & xmldoc.parseerror.reason & "<BR>"
  Response.End 
End If
select case format
case "csv" xslfile = "db2csvb.xsl"
case "pxml" xslfile = "prettyxml.xsl"
case else
xslfile = "results.xsl"
end select
xsldoc.Load Server.MapPath(xslfile)

'Check for a successful load of the XSL Document.
if xsldoc.parseerror.errorcode <> 0 then 
  Response.Write "Error loading XSL Document :" & "<BR>"
  Response.Write "----------------------------" & "<BR>"
  Response.Write "Error Code : " & xsldoc.parseerror.errorcode & "<BR>"
  Response.Write "Reason : " & xsldoc.parseerror.reason & "<BR>"
  Response.End 
End If
'finding first and last!
'set authorlist = xmldoc.getElementsByTagName("authorkey")
'set yearlist = xmldoc.getElementsByTagName("year")
'for i = 0 to authorlist.length-1
'response.write(yearlist.item(i).xml & " : " &  authorlist.item(i).xml & "<BR>")
'next
'Response.Write authorlist.length & " Records matched your query"
Response.Write xmldoc.TransformNode(xsldoc)
end if
else
Response.Write("No records match" & "<br/>" & strQuery)
end if
objRS.Close
objConn.Close
Set objRS = Nothing
Set objConn = Nothing
blnRetrieveData = False
%>
