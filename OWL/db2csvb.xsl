<xsl:stylesheet  version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><!--Output csv from a database retrieval-->
<xsl:output method="text" encoding="ISO-8859-1"/>
<xsl:strip-space elements="*"/>
<xsl:template match="/">
<xsl:apply-templates/>  
</xsl:template>
<xsl:template match="root">
<xsl:message>
      got root
</xsl:message>
<!-- must define record headings here in correct order!!-->
<xsl:text>"recordid","authorkey","author","year","title","serial","volume","page","fpage","address","abstract","abstractor","keywords","source","inserted","oktoshow","recordstatus"
</xsl:text>
<!-- imported text here -->
<xsl:for-each  select="*">
<!--tagname above depends on table name -->
<xsl:text>"</xsl:text><xsl:value-of select="recordid"/><xsl:text>","</xsl:text>
<xsl:value-of select="normalize-space(authorkey)"/><xsl:text>","</xsl:text>
<xsl:value-of select='normalize-space(author)'/><xsl:text>","</xsl:text>
<xsl:value-of select="year"/><xsl:text>","</xsl:text>
<xsl:value-of select='normalize-space(title)'/><xsl:text>","</xsl:text>
<xsl:value-of select="normalize-space(serial)"/><xsl:text>","</xsl:text>
<xsl:value-of select="volume"/><xsl:text>","</xsl:text>
<xsl:value-of select="spage"/><xsl:text>","</xsl:text>
<xsl:value-of select="fpage"/><xsl:text>","</xsl:text>
<xsl:value-of select="normalize-space(address)"/><xsl:text>","</xsl:text>
<xsl:value-of select="normalize-space(abstract)"/><xsl:text>","</xsl:text>
<xsl:value-of select="abstractor"/><xsl:text>","</xsl:text>
<xsl:value-of select="normalize-space(keywords)"/><xsl:text>","</xsl:text>
<xsl:value-of select="normalize-space(source)"/><xsl:text>","</xsl:text>
<xsl:value-of select="inserted"/><xsl:text>","</xsl:text>
<xsl:value-of select="oktoshow"/><xsl:text>","</xsl:text>
<xsl:value-of select="recordstatus"/><xsl:text>"&#10;</xsl:text>
</xsl:for-each>
 </xsl:template>
  </xsl:stylesheet>	
  