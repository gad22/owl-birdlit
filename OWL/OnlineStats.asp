<%@ Language=VBScript %>
<%

	'** ANTI CACHING CODE **
	Response.Expires = 60
	Response.Expiresabsolute = Now() - 1
	Response.AddHeader "pragma","no-cache"
	Response.AddHeader "cache-control","private"
	Response.CacheControl = "no-cache"

	FormAction = ""	
	haveRecs = false

	if request.form("search") = "Yes" or request.form("delete") = "Yes" then
		set con = server.CreateObject("ADODB.Connection")
		con.Provider = "sqloledb"
		con.Properties("Network Library") = "DBMSSOCN"
		con.Properties("Data Source") = "esql2k501.discountasp.net"
		con.Properties("Initial Catalog") = "SQL2005_544397_owl"
		con.Properties("User ID") = "SQL2005_544397_owl_user"
		con.Properties("Password") = "roltits"
		con.Open

	end if
%>
<html>
<head>
<script language="javascript">
	function SubmitQuery() {
		var El = document.getElementById("OutputType");
		if(El.options[El.selectedIndex].value == 0) 
			document.form1.action = "";
	
		if(El.options[El.selectedIndex].value == 1) 
			document.form1.action = "OnlineStatsDownload.asp";

		document.getElementById("search").value = "Yes";
		document.form1.submit();
	}
	
	function DeleteSelected() {
		if(confirm("Are you sure you want to delete the selected records?")) {
			document.form1.action = "";
			document.getElementById("delete").value = "Yes";
			document.form1.submit();
		}
	}
</script>

<style type="text/css">
	body { background-color:#f3f3f3; }
	table.outtbl td
	{
		background-color:white;
		border: solid 1px black;
		padding:2px 4px 2px 2px;
		font:10pt arial;
	}
	table.outtbl th
	{
		background-color:#dddddd;
		border: solid 1px black;
		padding:2px 4px 2px 2px;
		font:bold 10pt arial;
	}

</style>
</head>
<body>
<span style="font:12pt arial black;">OWL Online Data Entry Statistics</span><br/><br/>

<form name="form1" method="post" action="<% response.write(FormAction) %>">
<input type="hidden" id="search" name="search" />
<input type="hidden" id="delete" name="delete" />
<fieldset>
<legend>Enter optional filtering parameters</legend>
<table>
<tr>
	<td>Submitter Name:</td>
	<td><select id="qtype" name="qtype">
				<option value="equal" <% if request.form("qtype") = "equal" then response.write("selected") end if %>>is</option>
				<option value="starts" <% if request.form("qtype") = "starts" then response.write("selected") end if %>>starts with</options>
				<option value="contains" <% if request.form("qtype") = "contains" then response.write("selected") end if %>>contains</option>
			      </select> 

	</td>
	<td><input type="text" id="FilterName" name="FilterName" value="<% response.write(request.form("FilterName")) %>"/></td>
</tr>
<tr><td>Submit Date:</td><td><input type="text" id="SubmitDate" name="SubmitDate" value="<% response.write(request.form("SubmitDate")) %>" style="width:70px"/></td><td>&nbsp;</td></tr>
</table>
</fieldset>

<br/>
Output to: <select id="OutputType">
		<option value="0" <% if request.form("OutputType") = "0" then response.write("selected") end if %>>Screen</option>
		<option value="1" <% if request.form("OutputType") = "1" then response.write("selected") end if %>>Tab delimited file</option>
	</select>&nbsp;
<input type="button" id="SubmitQ" name="SubmitQ" value="Submit" onclick="SubmitQuery()" />
<br/><br/>

<table>
<tr>
<td valign="top">
<%
	if request.form("delete") = "Yes" then
		SubmitIDs = ""
		for x = 1 to Request.Form.count() 
        		key = Request.Form.key(x)
			if left(key,3) = "chk" and Request.Form.item(x) = "on" then
				submitid = mid(key,4)
				if len(SubmitIDs) > 0 then
					SubmitIDs = SubmitIDs & ","
				end if
				SubmitIDs = SubmitIDs & submitid
			end if 
    		next 
		if len(SubmitIDs) > 0 then
			SQLStr = "delete from onlinesubmitstats where submitid in(" & SubmitIDs & ")"
			con.Execute(SQLStr)
		end if
	end if

	if request.form("search") = "Yes" or request.form("delete") = "Yes" then
		strSQL = "select * from onlinesubmitstats"
		strWhere = ""
		if len(request.form("FilterName")) > 0 then
			qt = request.form("qtype")
			strName = request.form("FilterName")
			if qt = "equal" then
				strWhere = strWhere & " SubmitterName='" & replace(strName,"'","''") & "'"
			elseif	qt = "starts" then
				strWhere = strWhere & " SubmitterName like '" & replace(strName,"'","''") & "%'"
			elseif qt = "contains" then
				strWhere = strWhere & " SubmitterName like '%" & replace(strName,"'","''") & "%'"
			end if
			
		end if
		if not request.form("SubmitDate") is nothing then
			dt = request.form("SubmitDate")
			if Isdate(dt) then
				if len(strWhere) > 0 then
					strWhere = strWhere & " and "
				end if
				strWhere = strWhere & " SubmitDate='" & dt & "'"

			end if
		end if
		if len(strWhere) > 0 then
			strSQL = strSQL & " where" & strWhere
		end if
		strSQL = strSQL & " order by submitdate desc"

		set r = con.Execute(strSQL)
		if not r.eof then
			haveRecs = true
		else
			haveRecs = false
		end if

		if haveRecs Then

			Dim FldArr(4)
			FldArr(0) = "SubmitID"
			FldArr(1) = "SubmitterName"
			FldArr(2) = "SubmitterEmail"
			FldArr(3) = "SubmitDate"
			FldArr(4) = "SubmitCount"

			Response.Write("<table id='outtbl' class='outtbl' cellspacing=0 cellpadding=0>")
			Response.Write("<tr><td style='color:maroon;border:none;background-color:transparent;'>Mark for Delete</td>")
			For Each item In FldArr
	   			Response.Write("<th>" & item & "</th>")
			Next
			Response.Write("</tr>")
			while not r.eof
		   	Response.Write("<tr><td align='center'><input type='checkbox' name='chk" & r("SubmitID") & "'></td>")
		   	For Each item In FldArr
				if IsNull(r(item)) then
					Response.Write("<td>&nbsp;</td>")
				else
					val = trim(r(item))
					if len(val) = 0 then
				   	Response.Write("<td>&nbsp;</td>")
					else
				   	Response.Write("<td>" & val & "</td>")
					end if
				end if

		   	next
		  	 Response.Write("</tr>")
		   	r.movenext
			wend 
			Response.Write("</table>")
	
		else
			response.write("<span style='font:14pt arial black;color:maroon;'>No records found.</span>")
		end if
		con.close
		set con = nothing
	end if
%>
</td>
<td valign="top">
	<input type="button" id="DelSelected" value="Delete Selected" onclick="DeleteSelected()" style="display:<% if not haveRecs then response.write("none") end if %>";/>

</td>
</tr>
</table>

</form>
</body>
</html>

