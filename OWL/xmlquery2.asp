<%
private x
Function checkfield(pfname,pftype)
'fname is field name, ftype search type
'ftype=0 generates LIKE syntax
'ftype=1 generates CONTAINS(* syntax
'ftype=2 generates CONTAINS(fname, symtax
'ftype=3 generates = syntax
'ftype=4 allows operators and between syntax
'ftype=5 deals with dates
'ftype=6 special rules to deal with years
'need to alter fname and ftype if restrict to title only
if pfname="dog" and titonly="yes" then
    fname="title"
    ftype=2
else
    fname=pfname
    ftype=pftype
end if
param = request(pfname)
if x > 0 then
    xfilter = " AND "
else
    xfilter = " WHERE "
end if
if param <> "" then
    select case ftype
    case 1, 2
        if exact <> "yes" then
            param = ParseIt(param)
        end if
    case 4,6
        set opPat = new RegExp
        opPat.Pattern = "[<>=]"
        'if no operator change to type 0 instead
        if not opPat.Test(param) and Instr(param,"and")=0 then
            ftype = 0
        end if
        set opPat = nothing
    end select
    select case ftype
    case 0
        checkfield = xfilter & fname & " like '%" & param & "%'"
    case 1
        checkfield = xfilter & " CONTAINS(*,'" & param & "') "
    case 2
        checkfield = xfilter & " CONTAINS(" & fname & ",'" & param & "') "
    case 3
        checkfield = xfilter & fname & " = '" & param & "'"
    case 4
        checkfield = xfilter & fname & " " & param
    case 5
        checkfield = xfilter & fixdate(param,fname)
    case 6
        checkfield = xfilter & fixyear(param,fname)
    end select
    x = 1
else checkfield = ""
end if
end function

Function fixdate(rdate,fname)
'does type conversion for dates, preserving operator if there is one
if left(rdate,1)="<" or left(rdate,1)=">" or left(rdate,1)="=" then
oper = left(rdate,1)
stdate = right(rdate,len(rdate)-1)
else
oper = ""
stdate = rdate
end if
select case oper
case "<",">"
fixdate = fname & " " & oper & " convert(datetime,'" & stdate &"',103)"
case "=",""
fixdate = "convert(varchar," & fname & ",103) like '%" & stdate & "%'"
end select
end function
function fixyear(param,fname)
fixyear = "left(" & fname & ",4) " & param
end function
function checkerror(smess)
if err.number<> 0 then
'Clear response buffer
 Response.Clear
'Display Error Message to user
 response.Write("<HTML><HEAD><TITLE>OWL Database: Error</TITLE></HEAD>")
 response.Write("<BODY BGCOLOR='#C0C0C0'><FONT FACE='ARIAL'>An error occurred in the execution of this ASP page<br/>")
 response.Write("Message:" & smess & "<br/>")
 response.Write("<B>Error Object</B><br/>Error Number:" & Err.Number & "<br/>Error Description" & Err.Description)
 response.Write("<br/>Source " & Err.Source & "<br/>LineNumber " & Err.Line & "</FONT>")
 response.Write("<br/>===========================================================</BODY></HTML>")
 response.End
 err.clear
 end if
 return
 end function


Function ParseIt(sInput)
 'note this code found on technet via vbscript support page
 'corrects string quotes in most cases; modified by RHM to handle
 'multi word term near multi word term
      Dim strIn, RegEx
      strIn = sInput
      Set RegEx = New RegExp

      If Len(strIn) < 1 Then
           ParseIt = "You must enter a search string"
    Else
           strIn = Replace(strIn, Chr(34), "")
            If (InStr(strIn, "formsof") > 0) Or (InStr(strIn, "isabout") > 0)Then
            ParseIt = strIn
            Else
                  RegEx.IgnoreCase = True
                  RegEx.Global = True
                  RegEx.Pattern = "( and not | and | near )"
                  strIn = RegEx.Replace(strIn, Chr(34) & "$1" & Chr(34))
                  RegEx.Pattern = "( or not | or )"
                  strIn = RegEx.Replace(strIn, Chr(34) & "$1" & Chr(34))
                  strIn = Chr(34) & strIn & Chr(34)
                  ParseIt = strIn
         End If
       End If

End function

Dim objConn, objRS, strQuery
Dim strConnection, blnRetrieveData
on error resume next
'response.write "<br>Entering xmlquery <br/>"
Set objConn = Server.CreateObject("ADODB.Connection")
objConn.Open "Provider=sqloledb;Data Source=esql2k501.discountasp.net;Initial Catalog=SQL2005_544397_owl;User Id=SQL2005_544397_owl_user;Password=roltits"
'response.Write "<br>Connection ok<br/>"
'blnRetrieveData = False
'get data from the query form
format = request("format")
tabname = request("dbtable")
'response.Write " requested table is:" + tabname
'Response.Write " format is: " + format
'Response.End
if tabname = "dbo.OWLmaster" then
    authorkeytype=2
else
    authorkeytype=0
end if
exact = request.Form("exact")
'response.write "<br /> value of exact is:" + exact
titonly = request.Form("titonly")
'response.write " value of titonly is:" + titonly
qn = checkfield("rolcode",0)
qn = qn & checkfield("serial",0)
qn = qn & checkfield("volume",3)
qn = qn & checkfield("spage",3)
qn = qn & checkfield("authorkey",authorkeytype)
qn = qn & checkfield("year",6)
qn = qn & checkfield("dog",1)
qn = qn & checkfield("dog2",1)
qn = qn & checkfield("title",2)
qn = qn & checkfield("abstract",2)
qn = qn & checkfield("keywords",2)
qn = qn & checkfield("recordstatus",4)
qn = qn & checkfield("inserted",5)
qn = qn & checkfield("recordid",3)
qn = qn & checkfield("source",0)
qnord = " order by authorkey, year desc "
' original
'strQuery = "select * from " & tabname & qn & qnord & " for xml auto, elements"
strQuery = "select * from " & tabname & qn & qnord & " for xml auto, elements, type"
'checkerror(strQuery)
sedinf = request("edinf")
'response.write " value of sedinf is:" + sedinf
If sedinf = "yes"  and format = "txt" then
'response.write "<br/>" + qn + "<br/>"
end if
sincabs = request("incabs")
'response.write " value of incabs is:" + sincabs
'response.write " value of strQuery is:" + strQuery
Set objRS = objConn.Execute(strQuery)
'response.write " <p>AFTER objConn.Execute</p>"
'response.write "<p>EOF " & objRS.EOF & "</p>"
'response.write "<p>Fields.Count " & objRS.Fields.Count & "</p>"
'response.write "<p>Fields.Item(0).Name " & objRS.Fields.Item(0).Name & "</p>"
'response.write "<p>Fields.Item(0).UnderlyingValue " & objRS.Fields.Item(0).UnderlyingValue & "</p>"
'response.write "<p>Fields.Item(0).Type " & objRS.Fields.Item(0).Type & "</p>"
'response.write "<p>Properties.Count " & objRS.Properties.Count & "</p>"
'checkerror(strQuery)
if not objRS.EOF then
dim  regEx
result = objRS.GetString
'for debugging - server must be running VBscript 5.0 or higher
'response.write "VBscript " & ScriptEngine & "." & ScriptEngineMajorVersion & "." & ScriptEngineMinorVersion & "<br/>"
'following palaver needed because ODBC shoves in random \r at end of each 4k block!!
set regEx = new regexp
regEx.Global=true
regEx.Pattern="\r"
while regEx.Test(result)
'only attempt replacement if \r found should loop only once
'response.write "Found return in result string"
result = regEx.Replace(result,"")
wend
regEx = nothing
Dim xmldoc
Dim xsldoc


'response.write "result is: " + result

'Use the MSXML 4.0 Version dependent PROGID
'MSXML2.DOMDocument.4.0 if you wish to create
'an instance of the MSXML 4.0 DOMDocument object

Set xmldoc = Server.CreateObject("MSXML2.DOMDocument.4.0")
Set xsldoc = Server.CreateObject("MSXML2.DOMDocument.4.0")
xmlhdr = "<?xml version=""1.0"" encoding=""ISO-8859-1""?><root managestuff='"
xmlhdr = xmlhdr & sedinf & "' abstract='" & sincabs & "' format='" & format & "'>"
'bodge needed to deal with < in queries
qnreport = qn
set regex2 = new regexp
regex2.Global = true
regex2.Pattern="<"
while regex2.Test(qnreport)
qnreport = regex2.Replace(qnreport,"&lt;")
wend
regex2 = nothing
xmlhdr = xmlhdr & "<query>" & qnreport & "</query>"
result = xmlhdr & result & "</root>"
if format="xml" then
'Response.Write "<br/>Raw XML----------------------------" & "<BR>&#10;"
Response.ContentType = "text/xml"
Response.Write result
Response.End
else
xmldoc.loadXML( result)

'Check for a successful load of the XML Document.
if xmldoc.parseerror.errorcode <> 0 then
  Response.Write "Error loading XML Document :" & "<BR>"
  Response.Write "----------------------------" & "<BR>"
  Response.Write "Error Code : " & xmldoc.parseerror.errorcode & "<BR>"
  Response.Write "Reason : " & xmldoc.parseerror.reason & "<BR>"
  Response.End
End If
select case format
case "csv","tab"
xslfile = "db2csvd.xsl"
case "pxml" xslfile = "prettyxml.xsl"
case "xlxml" xslfile = "createspreadsheetML.xsl"
'Response.Write "xsl assigned - " & xslfile
'Response.End
case else
xslfile = "results.xsl"
end select
xsldoc.Load Server.MapPath(xslfile)

'Check for a successful load of the XSL Document.
if xsldoc.parseerror.errorcode <> 0 then
  Response.Write "Error loading XSL Document :" & "<BR>"
  Response.Write "----------------------------" & "<BR>"
  Response.Write "Error Code : " & xsldoc.parseerror.errorcode & "<BR>"
  Response.Write "Reason : " & xsldoc.parseerror.reason & "<BR>"
  Response.End
End If
'finding first and last!
'set authorlist = xmldoc.getElementsByTagName("authorkey")
'set yearlist = xmldoc.getElementsByTagName("year")
'for i = 0 to authorlist.length-1
'response.write(yearlist.item(i).xml & " : " &  authorlist.item(i).xml & "<BR>")
'next
'Response.Write authorlist.length & " Records matched your query"
'if err.number = 0 then
select case format
case "csv"
Response.ContentType = "text"
Response.AddHeader "Content-Disposition","attachment; filename=OWLrecords.csv"
case "tab"
Response.ContentType = "text"
Response.AddHeader "Content-Disposition","attachment; filename=OWLrecords.txt"
Response.write("============================ REMOVE HEADER =================================" & chr(10))
Response.write("QUERY PARAMETERS: " & qn & chr(10))
Response.write("=========================================================================================" & chr(10))
case "xlxml"
'Response.Write "xlxml type"
Response.ContentType = "text/xml"
Response.AddHeader "Content-Disposition","attachment; filename=OWLrecords.xml"
end select
Response.Write xmldoc.TransformNode(xsldoc)
'end if
end if
else
Response.Write("No records match" & "<br/>" & strQuery)
end if
objRS.Close
objConn.Close
Set objRS = Nothing
Set objConn = Nothing
blnRetrieveData = False
%>
