<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">

<xsl:output method="xml" version="1.0" omit-xml-declaration="no" indent="yes"/>
<xsl:template match="root">
<xsl:processing-instruction name="mso-application">progid="Excel.Sheet"</xsl:processing-instruction><Workbook>
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <LastAuthor>OWLdatabase</LastAuthor>
  <Created></Created>
  <LastSaved></LastSaved>
  <Version>11.5606</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>6225</WindowHeight>
  <WindowWidth>8610</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Tahoma"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s22">
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s28">
   <NumberFormat ss:Format="General Date"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="OWLrecords">
  <Table>
   <Column ss:AutoFitWidth="0" ss:Width="55.5"/>
   <Column ss:Width="47.25" ss:Span="254"/>
   <Row>
    <Cell><Data ss:Type="String">recordid</Data></Cell>
    <Cell><Data ss:Type="String">authorkey</Data></Cell>
    <Cell><Data ss:Type="String">author</Data></Cell>
    <Cell><Data ss:Type="String">year</Data></Cell>
    <Cell><Data ss:Type="String">title</Data></Cell>
    <Cell><Data ss:Type="String">serial</Data></Cell>
    <Cell><Data ss:Type="String">volume</Data></Cell>
    <Cell><Data ss:Type="String">spage</Data></Cell>
    <Cell><Data ss:Type="String">fpage</Data></Cell>
    <Cell><Data ss:Type="String">address</Data></Cell>
    <Cell><Data ss:Type="String">abstract</Data></Cell>
    <Cell><Data ss:Type="String">abstractor</Data></Cell>
    <Cell><Data ss:Type="String">keywords</Data></Cell>
    <Cell><Data ss:Type="String">source</Data></Cell>
    <Cell><Data ss:Type="String">inserted</Data></Cell>
    <Cell><Data ss:Type="String">oktoshow</Data></Cell>
    <Cell><Data ss:Type="String">recordstatus</Data></Cell>
   </Row>
<xsl:apply-templates select="dbo.OWLmaster"/>
 </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <Print>
    <Gridlines/>
   </Print>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>1</ActiveRow>
     <ActiveCol>1</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
</xsl:template>
<xsl:template match="dbo.OWLmaster">
   <Row>
    <Cell><Data ss:Type="Number"><xsl:value-of select="recordid"/></Data></Cell>
    <Cell><Data ss:Type="String"><xsl:value-of select="normalize-space(authorkey)"/></Data></Cell>
    <Cell><Data ss:Type="String"><xsl:value-of select="normalize-space(author)"/></Data></Cell>
    <Cell><Data ss:Type="String"><xsl:value-of select="year"/></Data></Cell>
    <Cell><Data ss:Type="String"><xsl:value-of select="normalize-space(title)"/></Data></Cell>
   <Cell><Data ss:Type="String"><xsl:value-of select="normalize-space(serial)"/></Data></Cell>
    <Cell><Data ss:Type="String"><xsl:value-of select="volume"/></Data></Cell>
<Cell><Data ss:Type="String"><xsl:value-of
select="spage"/></Data></Cell><!--should be a number but db has some
garbage; types here match actual types in OWLmaster-->
    <Cell><Data ss:Type="String"><xsl:value-of select="fpage"/></Data></Cell>
    <Cell><Data ss:Type="String"><xsl:value-of select="normalize-space(address)"/></Data></Cell>
    <Cell><Data ss:Type="String"><xsl:value-of select="normalize-space(abstract)"/></Data></Cell>
    <Cell><Data ss:Type="String"><xsl:value-of select="abstractor"/></Data></Cell>
    <Cell><Data ss:Type="String"><xsl:value-of select="normalize-space(keywords)"/></Data></Cell>
   <Cell><Data ss:Type="String"><xsl:value-of select="normalize-space(source)"/></Data></Cell>
    <Cell ss:StyleID="s28"><Data ss:Type="DateTime"><xsl:value-of select="inserted"/>
</Data></Cell>
<Cell><Data ss:Type="Number"><xsl:value-of select="oktoshow"/></Data></Cell> 
    <Cell><Data ss:Type="Number"><xsl:value-of select="recordstatus"/></Data></Cell>
</Row>
</xsl:template>
</xsl:stylesheet>
