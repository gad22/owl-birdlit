<?xml version="1.0"?>  
  <xsl:stylesheet  version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt">
  <xsl:template match="/">
  <HTML>
    <HEAD>
    <META http-equiv="Content-Type"
    content="text/html; charset=iso-8859-1"/>
    <title>OWL Database results</title>
     <LINK HREF="owl.css" REL="stylesheet" TYPE="text/css"/>
    </HEAD>
       <BODY
       BACKGROUND="/samples/images/backgrnd.gif" 
       bgcolor="#FFFFFF"> 
	 
<H1><B>OWL database search results</B></H1> 
<h4><xsl:value-of select="count(//authorkey)"/> Records matched your query</h4>
<HR/> <div class="maincontent">
<xsl:apply-templates/>
</div>
</BODY></HTML>
  </xsl:template>
  <xsl:template match="root">
<xsl:for-each  select="*">
<!-- changed from dbo.OWLmaster so it works with all tables -->
<p><div class="controls">
Article #
<!--tagname above depends on table name -->
<xsl:value-of select="recordid"/>
<xsl:if test="../@managestuff='yes'">
  Source:<xsl:value-of select="source"/> 
  Authorkey: <xsl:value-of select="authorkey"/> 
 Keywords:  <xsl:value-of select="keywords"/><br/>
Accession date: <xsl:value-of select="substring-before(inserted,'T')"/>
 at <xsl:value-of select="substring-before(substring-after(inserted,'T'),'.')"/>
 Ok to display= <xsl:value-of select="oktoshow"/>
 Record status= <xsl:value-of select="recordstatus"/>
 </xsl:if></div>
<b><xsl:value-of select="author"/></b><xsl:text> </xsl:text> 
<xsl:value-of select="year"/>
<xsl:text>. </xsl:text>
<br/>
<span style="font: italic"><xsl:value-of select="title"/><xsl:text> </xsl:text>
</span>
<xsl:value-of select="serial"/><xsl:text> </xsl:text>
<span style="font: bold"> <xsl:value-of select="volume"/>: </span>
<xsl:value-of select="spage"/>-<xsl:value-of select="fpage"/>
<xsl:if test="address">
[<span style="font:italic"> <xsl:value-of select="address"/></span>]
</xsl:if>
<xsl:if test="../@abstract='yes'">
            <br/><xsl:value-of select="abstract"/><br/>
            <xsl:value-of select="abstractor"/>
  </xsl:if>
          </p></xsl:for-each>
</xsl:template>

</xsl:stylesheet>
