<?xml version="1.0"?>  
  <xsl:stylesheet  version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt">
  <xsl:template match="/">
  <HTML>
    <HEAD>
    <META http-equiv="Content-Type"
    content="text/html; charset=iso-8859-1"/>
    <title>OWL Database results</title>
     <LINK HREF="owl.css" REL="stylesheet" TYPE="text/css"/>
    </HEAD>
       <BODY
       BACKGROUND="/samples/images/backgrnd.gif" 
       bgcolor="#FFFFFF"> 
	 
<H1><B>OWL database search results</B></H1> 
<h4><xsl:value-of select="count(//authorkey)"/> Records matched your query<xsl:apply-templates select="//query"/></h4>
<HR/> <div class="maincontent">
<xsl:apply-templates/>
</div><script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-381560-11");
pageTracker._trackPageview();
} catch(err) {}</script>
</BODY></HTML>
  </xsl:template>
  <xsl:template match="root">
  
<xsl:for-each  select="*">
<!-- changed from dbo.OWLmaster so it works with all tables -->
<xsl:if test="not(local-name()='query')">
<p><div class="controls">
Article #
<!--tagname above depends on table name -->
<xsl:value-of select="recordid"/>
<xsl:if test="../@managestuff='yes'">
  Source:<xsl:value-of select="source"/> 
  Authorkey: <xsl:value-of select="authorkey"/> 
<!-- Keywords:  <xsl:value-of select="keywords"/>--><br/>
Accession date: <xsl:value-of select="substring-before(inserted,'T')"/>
 at <xsl:value-of select="substring-before(substring-after(inserted,'T'),'.')"/>
 Ok to display= <xsl:value-of select="oktoshow"/>
 Record status= <xsl:value-of select="recordstatus"/>
 </xsl:if></div>
<b><xsl:value-of select="author"/></b><xsl:text> </xsl:text> 
<xsl:value-of select="year"/>
<xsl:text>. </xsl:text>
<br/>
<span style="font: italic"><xsl:value-of select="title"/><xsl:text> </xsl:text>
</span>
<xsl:value-of select="serial"/><xsl:text> </xsl:text>
<span style="font: bold"> <xsl:value-of select="volume"/>: </span>
<xsl:value-of select="spage"/>-<xsl:value-of select="fpage"/>
<xsl:if test="address">
[<span style="font:italic"> <xsl:apply-templates select="address"/></span>]
</xsl:if>
<xsl:if test="email">
<xsl:element name = "a">
<xsl:attribute name="href">mailto:
<xsl:value-of select="email"/>
</xsl:attribute>
<xsl:value-of select="email"/>
</xsl:element>
</xsl:if>
<xsl:if test="../@abstract='yes'">
            <br/><xsl:value-of select="abstract"/><br/><br/>
<small>Keywords: <xsl:value-of select="keywords"/><br/>
            <xsl:value-of select="abstractor"/></small>
  </xsl:if>
          </p></xsl:if></xsl:for-each>
</xsl:template>

<xsl:template match="a">
<xsl:comment> prob redundant </xsl:comment>
<xsl:element name = "a">
<xsl:attribute name="href">
<xsl:value-of select="@href"/>
</xsl:attribute>
<xsl:value-of select="."/>
</xsl:element>
</xsl:template>
<xsl:template match="query">
<!--<xsl:if test="../@managestuff='yes'">-->
<br/><xsl:value-of select="."/>
<!--</xsl:if>-->
</xsl:template>
</xsl:stylesheet>
