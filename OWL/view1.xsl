<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:my="http://schemas.microsoft.com/office/infopath/2003/myXSD/2004-01-18T16:19:11" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xd="http://schemas.microsoft.com/office/infopath/2003" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:xdExtension="http://schemas.microsoft.com/office/infopath/2003/xslt/extension" xmlns:xdXDocument="http://schemas.microsoft.com/office/infopath/2003/xslt/xDocument" xmlns:xdSolution="http://schemas.microsoft.com/office/infopath/2003/xslt/solution" xmlns:xdFormatting="http://schemas.microsoft.com/office/infopath/2003/xslt/formatting" xmlns:xdImage="http://schemas.microsoft.com/office/infopath/2003/xslt/xImage">
	<xsl:output method="html" indent="no"/>
	<xsl:template match="root">
		<html>
			<head>
				<style tableEditor="TableStyleRulesID">TABLE.xdLayout TD {
	BORDER-RIGHT: medium none; BORDER-TOP: medium none; BORDER-LEFT: medium none; BORDER-BOTTOM: medium none
}
TABLE {
	BEHAVIOR: url (#default#urn::tables/NDTable)
}
TABLE.msoUcTable TD {
	BORDER-RIGHT: 1pt solid; BORDER-TOP: 1pt solid; BORDER-LEFT: 1pt solid; BORDER-BOTTOM: 1pt solid
}
</style>
				<meta http-equiv="Content-Type" content="text/html"></meta>
				<style controlStyle="controlStyle">BODY{margin-left:21px;color:windowtext;background-color:window;layout-grid:none;} 		.xdListItem {display:inline-block;width:100%;vertical-align:text-top;} 		.xdListBox,.xdComboBox{margin:1px;} 		.xdInlinePicture{margin:1px; BEHAVIOR: url(#default#urn::xdPicture) } 		.xdLinkedPicture{margin:1px; BEHAVIOR: url(#default#urn::xdPicture) url(#default#urn::controls/Binder) } 		.xdSection{border:1pt solid #FFFFFF;margin:6px 0px 6px 0px;padding:1px 1px 1px 5px;} 		.xdRepeatingSection{border:1pt solid #FFFFFF;margin:6px 0px 6px 0px;padding:1px 1px 1px 5px;} 		.xdBehavior_Formatting {BEHAVIOR: url(#default#urn::controls/Binder) url(#default#Formatting);} 	 .xdBehavior_FormattingNoBUI{BEHAVIOR: url(#default#CalPopup) url(#default#urn::controls/Binder) url(#default#Formatting);} 	.xdExpressionBox{margin: 1px;padding:1px;word-wrap: break-word;text-overflow: ellipsis;overflow-x:hidden;}.xdBehavior_GhostedText,.xdBehavior_GhostedTextNoBUI{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#TextField) url(#default#GhostedText);}	.xdBehavior_GTFormatting{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#Formatting) url(#default#GhostedText);}	.xdBehavior_GTFormattingNoBUI{BEHAVIOR: url(#default#CalPopup) url(#default#urn::controls/Binder) url(#default#Formatting) url(#default#GhostedText);}	.xdBehavior_Boolean{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#BooleanHelper);}	.xdBehavior_Select{BEHAVIOR: url(#default#urn::controls/Binder) url(#default#SelectHelper);}	.xdRepeatingTable{BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none; BORDER-COLLAPSE: collapse; WORD-WRAP: break-word;}.xdTextBox{display:inline-block;white-space:nowrap;text-overflow:ellipsis;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow:hidden;text-align:left;} 		.xdRichTextBox{display:inline-block;;padding:1px;margin:1px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow-x:hidden;word-wrap:break-word;text-overflow:ellipsis;text-align:left;font-weight:normal;font-style:normal;text-decoration:none;vertical-align:baseline;} 		.xdDTPicker{;display:inline;margin:1px;margin-bottom: 2px;border: 1pt solid #dcdcdc;color:windowtext;background-color:window;overflow:hidden;} 		.xdDTText{height:100%;width:100%;margin-right:22px;overflow:hidden;padding:0px;white-space:nowrap;} 		.xdDTButton{margin-left:-21px;height:18px;width:20px;behavior: url(#default#DTPicker);} 		.xdRepeatingTable TD {VERTICAL-ALIGN: top;}</style>
				<style languageStyle="languageStyle">BODY {
	FONT-SIZE: 10pt; FONT-FAMILY: Verdana
}
TABLE {
	FONT-SIZE: 10pt; FONT-FAMILY: Verdana
}
SELECT {
	FONT-SIZE: 10pt; FONT-FAMILY: Verdana
}
.optionalPlaceholder {
	PADDING-LEFT: 20px; FONT-WEIGHT: normal; FONT-SIZE: xx-small; BEHAVIOR: url(#default#xOptional); COLOR: #333333; FONT-STYLE: normal; FONT-FAMILY: Verdana; TEXT-DECORATION: none
}
.langFont {
	FONT-FAMILY: Verdana
}
</style>
			</head>
			<body>
				<div><xsl:apply-templates select="dbo.OWLmaster" mode="_1"/>
				</div>
				<div><xsl:apply-templates select="dbo.OWLmaster" mode="_3"/>
				</div>
				<div><xsl:apply-templates select="dbo.OWLmaster" mode="_4"/>
				</div>
				<div><xsl:apply-templates select="dbo.OWLmaster" mode="_5"/>
				</div>
				<div>Managestuff: <select class="xdComboBox xdBehavior_Select" title="" size="1" xd:xctname="DropDown" xd:CtrlId="CTRL11" tabIndex="0" xd:binding="@managestuff" xd:boundProp="value" style="WIDTH: 130px">
						<xsl:attribute name="value">
							<xsl:value-of select="@managestuff"/>
						</xsl:attribute>
						<option value="">
							<xsl:if test="@managestuff=&quot;&quot;">
								<xsl:attribute name="selected">selected</xsl:attribute>
							</xsl:if>Select...</option>
						<option value="yes">
							<xsl:if test="@managestuff=&quot;yes&quot;">
								<xsl:attribute name="selected">selected</xsl:attribute>
							</xsl:if>yes</option>
					</select>
				</div>
				<div>And a bit of steering commentary</div>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="dbo.OWLmaster" mode="_1">
		<div class="xdRepeatingSection xdRepeating" title="" style="MARGIN-BOTTOM: 6px; WIDTH: 542px" align="left" xd:xctname="RepeatingSection" xd:CtrlId="CTRL1" tabIndex="-1">
			<div>Author: <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL2" tabIndex="0" xd:binding="author" style="WIDTH: 130px">
					<xsl:value-of select="author"/>
				</span>
			</div>
			<div> </div>
			<div> </div>
			<div> </div>
		</div>
	</xsl:template>
	<xsl:template match="dbo.OWLmaster" mode="_3">
		<div class="xdRepeatingSection xdRepeating" title="" style="MARGIN-BOTTOM: 6px; WIDTH: 542px" align="left" xd:xctname="RepeatingSection" xd:CtrlId="CTRL5" tabIndex="-1">
			<div>Serial: <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL6" tabIndex="0" xd:binding="serial" style="WIDTH: 130px">
					<xsl:value-of select="serial"/>
				</span>
			</div>
			<div> </div>
			<div> </div>
			<div> </div>
		</div>
	</xsl:template>
	<xsl:template match="dbo.OWLmaster" mode="_4">
		<div class="xdRepeatingSection xdRepeating" title="" style="MARGIN-BOTTOM: 6px; WIDTH: 542px" align="left" xd:xctname="RepeatingSection" xd:CtrlId="CTRL7" tabIndex="-1">
			<div>Title: <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL8" tabIndex="0" xd:binding="title" style="WIDTH: 130px">
					<xsl:value-of select="title"/>
				</span>
			</div>
			<div> </div>
			<div> </div>
			<div> </div>
		</div>
	</xsl:template>
	<xsl:template match="dbo.OWLmaster" mode="_5">
		<div class="xdRepeatingSection xdRepeating" title="" style="MARGIN-BOTTOM: 6px; WIDTH: 542px" align="left" xd:xctname="RepeatingSection" xd:CtrlId="CTRL9" tabIndex="-1">
			<div>Authorkey: <span class="xdTextBox" hideFocus="1" title="" xd:xctname="PlainText" xd:CtrlId="CTRL10" tabIndex="0" xd:binding="authorkey" style="WIDTH: 130px">
					<xsl:value-of select="authorkey"/>
				</span>
			</div>
			<div> </div>
			<div> </div>
			<div> </div>
		</div>
	</xsl:template>
</xsl:stylesheet>
