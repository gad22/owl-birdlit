<%@ Language=VBScript %>
<%

	'** ANTI CACHING CODE **
	Response.Expires = 60
	Response.Expiresabsolute = Now() - 1
	Response.AddHeader "pragma","no-cache"
	Response.AddHeader "cache-control","private"
	Response.CacheControl = "no-cache"

	ShowForm = "block"
	ShowSave = "none"
	
	if request.form("hidSave") = "Yes" then
		set con = server.CreateObject("ADODB.Connection")
		con.Provider = "sqloledb"
		con.Properties("Network Library") = "DBMSSOCN"
		con.Properties("Data Source") = "esql2k501.discountasp.net"
		con.Properties("Initial Catalog") = "SQL2005_544397_owl"
		con.Properties("User ID") = "SQL2005_544397_owl_user"
		con.Properties("Password") = "roltits"
		con.Open
	
		SQLstr = "insert into owlinput " & _
		"(" & _
		"[source]," & _
		"[title]," & _
		"[author]," & _
		"[authorkey]," & _
		"[serial]," & _
		"[address]," & _
		"[email]," & _
		"[abstractor]," & _
		"[keywords]," & _
		"[year]," & _
		"[volume]," & _
		"[spage]," & _
		"[fpage]," & _
		"[abstract]," & _
		"[inserted]," & _
		"[submittername]," & _
		"[submitteremail]," & _
		"[submitdate]" & _
		")" & _
		" values ('OWLO','" & _
		CleanSQLStr(Request.Form("title")) & "',' " & _
		CleanSQLStr(Request.Form("author1")) & "','" & _
		CleanSQLStr(Request.Form("author1")) & "','" & _
		CleanSQLStr(Request.Form("journal")) & "','" & _
		CleanSQLStr(Request.Form("address")) & "','" & _
		CleanSQLStr(Request.Form("email")) & "','" & _
		ucase(CleanSQLStr(Request.Form("initials"))) & "','" & _
		CleanSQLStr(Request.Form("keywords")) & "','" & _
		CleanSQLStr(Request.Form("yearx")) & "','" & _
		CleanSQLStr(Request.Form("volume")) & "','" & _
		CleanSQLStr(Request.Form("spage")) & "','" & _
		CleanSQLStr(Request.Form("fpage")) & "','" & _
		CleanSQLStr(Request.Form("abstract")) & "','" & _
		Now() & "','" & _
		CleanSQLStr(Request.Form("hidOwlUser")) & "','" & _
		CleanSQLStr(Request.Form("hidOwlemail")) & "','" & _
		Date() & "'" & _
		")"
		con.Execute(SQLStr)
		ShowSave = "block"
		ShowForm = "none"

		con.close
		set con = nothing
	end if

	if request.form("hidClearCookies") = "Yes" then
		Response.Cookies("Owl").Expires = date() - 1
		LogInFormHidden = "''"
	else
		user = request.cookies("Owl")("User")
		email = request.cookies("Owl")("email")
		sessiondate = request.cookies("Owl")("SessionDate")

	
		if user = "" and len(request.form("hidOwlUser")) = 0 then
	   	   if request.form("hidNewLogin") = "Yes" then
			response.cookies("Owl")("User") = request.form("UserFirstName") & " " & request.form("UserLastName") 
			Response.Cookies("Owl").Expires = date() + 90
			response.cookies("Owl")("email") = request.form("UserEmail")
			response.cookies("Owl")("SessionDate") = date()
			user = request.form("UserFirstName") & " " & request.form("UserLastName")
			email = request.form("UserEmail")
			sessiondate = date()
			LogInFormHidden = "none"
	   	   else
           		LogInFormHidden = "block"
	   	   end if
		else
	   	   LogInFormHidden = "none"
		end if
	end if

	function FixDBLQuotes(strIn)
		NewStr = Replace(strIn,chr(34),"&quot;")
		FixDBLQuotes = NewStr
	end function

	function CleanSQLStr(strIn)
		NewStr = Replace(strIn,"'","''")
		CleanSQLStr = NewStr
	end function
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
<TITLE>OWL On-Line data input page</TITLE><LINK href="owl.css" type=text/css rel=stylesheet >
<link href="OwlInput.css" rel="stylesheet" type="text/css" />
<script language=JavaScript>
   //window.onscroll = ScrollIntoView;

   function ScrollIntoView() { 
	document.getElementById("Overlay").style.top = f_scrollTop();
   }


   function f_scrollTop() {
	return f_filterResults (
		window.pageYOffset ? window.pageYOffset : 0,
		document.documentElement ? document.documentElement.scrollTop : 0,
		document.body ? document.body.scrollTop : 0
	);
   }
   
   function f_filterResults(n_win, n_docel, n_body) {
	var n_result = n_win ? n_win : 0;
	if (n_docel && (!n_result || (n_result > n_docel)))
		n_result = n_docel;
	return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
   }


   function getScrollHeight()
   {
      var h = window.pageYOffset ||
           document.body.scrollTop ||
           document.documentElement.scrollTop;
           
      return h ? h : 0;
      
   }  

   function ValidateLogin() {
	if(!document.getElementById("UserFirstName").value) {
		alert("Please enter your first name.")
		return;
	}
	if(!document.getElementById("UserLastName").value) {
		alert("Please enter your last name.")
		return;
	}

	if(!document.getElementById("UserEmail").value) {
		alert("Please enter your email address. If you don't have one enter 'none'.")
		return;
	}
	document.getElementById("hidNewLogin").value = "Yes";
	document.forms[0].submit();
   }

   function Save() {
	if(!document.getElementById("author1").value) {
		alert("Please enter the author.");
		return;
	}	

	var yearx = document.getElementById("yearx").value;
	if(yearx.length == 0) {
		alert("Please enter the year of publication.");
		return;
	} else {
		if(!IsInt(yearx)) {
			alert("Please enter a valid publication year.");
			return;
		}
	}
	
	if(!document.getElementById("title").value) {
		alert("Please enter the title.");
		return;
	}

	if(!document.getElementById("journal").value) {
		alert("Please enter a journal or serial name.");
		return;
	}

	if(!document.getElementById("volume").value) {
		alert("Please enter the volume.");
		return;
	}

	if(!document.getElementById("spage").value) {
		alert("Please enter the start page.");
		return;
	}

	if(!document.getElementById("fpage").value) {
		alert("Please enter the finish page.");
		return;
	}

	if(!document.getElementById("address").value) {
		alert("Please enter the author address.");
		return;
	}

	if(!document.getElementById("initials").value) {
		alert("Please enter your initials.");
		return;
	}

	if(!document.getElementById("keywords").value) {
		alert("Please enter your keywords.");
		return;
	}


	document.getElementById("hidSave").value = "Yes";
	document.forms[0].submit();
   }


   function EnterMore() {
	document.getElementById("hidSave").value = "";
	document.forms[0].submit();
   }

   function Quit() {
	location.replace("advancedsearch.htm");
   }
 
   function ClearCookies() {
	document.getElementById("hidClearCookies").value = "Yes";
	document.forms[0].submit();
   }

   function IsInt(NumTxt) {
	var strLen = NumTxt.length;
	for(var i=0; i < strLen; i++)
	{
		var charnum = NumTxt.charCodeAt(i);
		if(!(charnum > 47 & charnum < 58))
			return false;
	}
	return true;
   }

   function ClearForm() {
	document.getElementById("author1").value = "";
	document.getElementById("title").value = "";
	document.getElementById("yearx").value = "";
	document.getElementById("journal").value = "";
	document.getElementById("volume").value = "";
	document.getElementById("spage").value = "";
	document.getElementById("fpage").value = "";
	document.getElementById("address").value = "";
	document.getElementById("initials").value = "";
	document.getElementById("keywords").value = "";
	document.getElementById("abstract").innerHTML = "";

   }


   function getfields() {
  	with(document.indata) {
  		au = author1.value; yr = yearx.value; jn = journal.value; vol = volume.value;
 		db=dbtable.options[dbtable.selectedIndex].value; sp = spage.value;
  	}
   }
  
   function nodata()  {
	nd=(au==''&yr==''&jn==''&vol==''&db!='OWLinput')
  	if(nd){alert('Must supply search condition for this database: '+ db)}
  	return nd
   }
  
   function dbcall() {
  	parms = 'incheck=yes\&dbtable='+db+'\&authorkey='+au+'\&year='+yr+'\&serial='+jn+'\&volume='+vol
  	parms += '\&spage='+sp+'\&edinf=yes\&incabs=yes'
  	return parms
   }
 
</script>
</HEAD>
<BODY>
<form name="indata" id="indata" method="post">
<input type="hidden" id="hidOwlUser" name="hidOwlUser" value="<% response.write(FixDBLQuotes(user)) %>"/>
<input type="hidden" id="hidOwlemail" name="hidOwlemail" value="<% response.write(FixDBLQuotes(email)) %>"/>
<input type="hidden" id="hidOwlSessionDate" name="hidOwlSessionDate" value="<% response.write(FixDBLQuotes(sessiondate)) %>"/>
<input type="hidden" id="hidNewLogin" name="hidNewLogin" />
<input type="hidden" id="hidClearCookies"name="hidClearCookies"/>
<input type="hidden" id="hidSave" name="hidSave" />

<table style="display:<% response.write(ShowForm) %>;">
   <tr>
      <td colspan="2"><% if len(user) > 0 then response.write("Welcome, " & user & "<span id='ClearCookies' onclick=ClearCookies() style='margin-left:60px;font: 8pt arial;color: blue;text-decoration: underline;cursor: hand;'>Clear Submitter Identity</span>") end if%></td></tr>
   </tr>
   <tr>
    <td>
	<IMG height=150 alt="Great Saphirewing" src="GSW.jpg" width=150 align=right border=1 >
    </td>
    <td vAlign=top>
      <H1>OWL On-Line data input</H1>
      <p class=longtext>Fill in the fields below for each 
      article, check the information&nbsp;and then press "submit". To check if 
      record is already in database, enter some or all of Author, Year, Journal 
      and Volume (NB not Title or Abstract terms) and press "Check"; make search terms general to ensure a hit if 
      the record is there. Data are moved from input to production table each 
      night at midnight UK time; topic searches should work correctly the day 
      after entry.</p>
      <p class=longtext> Normal service ....</p>
  </td>
  </tr>
  <tr>
    <td class=controls vAlign=top width=160>
      <div>
      <h3><A href="default.htm" >Home</A></h3>
      <h3>Data Input</h3><INPUT type="button" value="Submit" name="btnSave" onclick="Save()"> 
      <br><br><INPUT type=button value="Clear Form" onclick="ClearForm()"> 

      <h3>Checking</h3><INPUT id=button1 style="WIDTH: 88px; HEIGHT: 27px" onclick="getfields();if(nodata()){ } else {chkwin=window.open('xmlquery2.asp?'+dbcall(),'dbcheck');chkwin.focus()}" type=button size=29 value=check name=button1><br 
      ><STRONG>Database to 
      check:</STRONG> <SELECT style="WIDTH: 135px" name=dbtable 
      > <OPTION value=dbo.checkinput2 selected >All (no abstracts)</OPTION> <OPTION 
        value=dbo.OWLmaster>Production</OPTION> <OPTION 
        value=dbo.OWLinput>Input</OPTION></SELECT><br 
      ></div>
      <h3>Information</h3>
      <UL>
        <li><A href="2007guidelines.pdf" target=_BLANK >Abstractors 
        Guidlines</A> 
        <li><A href="sampledata.htm" target=_blank >Sample data</A> 
        <LI><A href="notesonsamples.htm" target=_blank >Notes on 
        samples</A> 
        <li><A href="ROLsubjectcodes2.htm" target=blank >Subject 
        Codes</A></li></UL>
      <P>&nbsp;</P></td>
    <td class=inputfields vAlign=top>
      <div><B>Authors:</B> e.g. Bloggs 
      JR; Smith J - initials in capitals after a single space following the 
      surname, no spaces between initials; separate authors by a semi-colon 
      followed by a space, exactly as the example. Use only unaccented 
      characters for authors names. List all authors (no et.al. please)<br 
      ><INPUT class=inbox2 size=56 name="author1" id="author1"><br>
	<div style="margin-top:8px;">	
		<B>Year of publication:</B>
		<INPUT type="text" maxLength=4 size=4 name="yearx" id="yearx" value="<% response.write(FixDBLQuotes(request.form("yearx"))) %>">
	</div>
	<div style="margin-top:8px;">
		<B>Title:</B> Can be in any language, but please provide 
      		an English translation in [ ]. Accented characters will be stored ok. <br/>
		<INPUT type="text" class=inbox2 size=100 name="title" id="title"/>
	</div>
	<div style="margin-top:8px;">
		<b>Journal or serial:</b><br>
		<INPUT type="text" class=inbox1 size=50 name="journal" id="journal" value="<% response.write(FixDBLQuotes(request.form("journal"))) %>">
	</div>
	<div style="margin-top:8px;">	
		<B>Volume:</B><INPUT type="text" size=16 name="volume" id="volume" value="<% response.write(FixDBLQuotes(request.form("volume"))) %>">
		<B>Start page:</B><INPUT size=10 name="spage" id="spage">
		<B>Finish page:</B><INPUT size=10 name="fpage" id="fpage"/>
	</div>
      	<div style="margin-top:8px;">
		<B>Address:</B> Use the present address of the corresponding author. Abbreviate addresses and 
      		include only enough of the address to insure postal delivery to the 
      		author.<br/>
		<INPUT type="text" class=inbox1 size=90 name="address" id="address">
	</div>
      	<div style="margin-top:8px;">
		<b>Author's E-mail address:</b><br/>
      		<INPUT class=inbox1 size=75 name=email>
	</div>
      	<div style="margin-top:8px;">
		<B>Abstract:</B> Language and accented character rules apply as for the title field.<br/>
		<TEXTAREA name="abstract" id="abstract" rows=5 cols=70></TEXTAREA> 
	</div>
      	<div style="margin-top:8px;">
		<B>Your initials:</B> <INPUT type="text" size=8 name="initials" id="initials" value="<% response.write(FixDBLQuotes(request.form("initials"))) %>">
	</div>
	<div style="margin-top:8px;">
		<B>Keywords or <A href="ROLsubjectcodes2.htm" >subject 
      		codes:</A></B> e.g. predation; functional response - separate keywords by 
      		a semi-colon followed by a space, exactly as the example. Do not repeat 
      		words in the title or abstract but <b>do</b>include 
      		scientific names of all species mentioned (<u>genus</u> <u>species</u> 
      		<strong>do not abbreviate </strong>; ). Accents are 
      		not sensible here.<BR>
		<INPUT type="text" class=inbox2 size=75 id="keywords" name="keywords"/>
	</div> 
      <P class=longtext>Keep abstracts short (1- 3 
      sentences at most). Abstracts are not designed to summarise the article, 
      but rather to enable a reader to judge if an article is worth pursuing. 
      Use the active voice whenever possible. Eliminate clearly understood 
      subjects -e.g. "Describes 5 new species" is preferred over "Five new 
      species are described." Never use "there is" or "there are." Be concise, 
      but not telegraphic. Try not to repeat in the abstract anything that is in 
      the title. Some titles are in themselves summaries. Use the scientific 
      name in the abstract if the English name is in the title and vice versa. 
      <strong><u>Do not abbreviate scientific names</u></strong>. Also specify the language of the originator.  Please initial any abstract you write. 
      </P></div>
   </td>
   </tr>
</table>




<div id="Overlay" class="Overlay" style="display:<% response.write(LogInFormHidden) %>;">
    
</div>


<div id="LoginForm" style="display:<% response.write(LogInFormHidden) %>;" class="Login">
	<span style="font:bold 10pt arial;color:maroon;">Please enter your name and email address:</span><br/><br/><br/>
	<table>
	<tr><td>First Name:</td><td><input type="text" id="UserFirstName" name="UserFirstName"></td></tr>
	<tr><td>Last Name:</td><td><input type="text" id="UserLastName" name="UserLastName"></td></tr>
	<tr><td>email address:</td><td><input type="text" id="UserEmail" name="UserEmail"></td></tr>
	</table>
	<br/><br/>
	<input type="button" value="Submit" onclick="ValidateLogin()" style="float:right;"/>
	
</div>


<div id="SaveMessage" style="display:<% response.write(ShowSave) %>;font:14pt arial black;color:maroon;padding:100px;text-align:center;">
	
	Record saved sucessfully<br/><br/>
	<input type="button" onclick="EnterMore()" value="Enter More Records" style="margin-right:80px:"/>
	<input type="button" onclick="Quit()" value="Go back to search" />
	
</div>

</form>




</BODY>
</HTML>
