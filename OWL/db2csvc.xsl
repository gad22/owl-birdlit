<xsl:stylesheet  version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><!--Output csv from a database retrieval-->
<xsl:output method="text" encoding="ISO-8859-1"/>
<xsl:strip-space elements="*"/>
<xsl:template match="/">
<xsl:apply-templates/>  
</xsl:template>
<xsl:template match="root">
  <xsl:variable name="mid">
<xsl:choose>
  <xsl:when test="@format='csv'">
    <xsl:text>","</xsl:text>
</xsl:when>
<xsl:otherwise>
  <xsl:text>&#9;</xsl:text> 
</xsl:otherwise>
    </xsl:choose>    
  </xsl:variable>
<xsl:message>
  got root 
</xsl:message>

<!-- set up delimiters depending on format: default to .tab-->
<xsl:text>To export this information as a delimited file:&#10;
Right click on this screen and select 'View Source'.</xsl:text><br/> 
<xsl:text>This should cause the
file to open in a text editor (Notepad or WordDoc in Windows systems, 
depending on how long it is. Once in notepad delete all text above the line starting "recordid" below&#10;
Then SaveAs filename.csv for Comma delimited (.csv) text or filename.dat for tab delimited.&#10;
&lt;Snip&gt;&#10;
------------------------------------------------------&#10;
</xsl:text>
<!-- must put record headings in correct order here-->
<xsl:if test="@format='csv'"><xsl:text>"</xsl:text></xsl:if>
<xsl:text>recordid</xsl:text><xsl:value-of select="$mid"/>
<xsl:text>authorkey</xsl:text><xsl:value-of select="$mid"/>
<xsl:text>author</xsl:text><xsl:value-of select="$mid"/>
<xsl:text>year</xsl:text><xsl:value-of select="$mid"/>
<xsl:text>title</xsl:text><xsl:value-of select="$mid"/>
<xsl:text>serial</xsl:text><xsl:value-of select="$mid"/>
<xsl:text>volume</xsl:text><xsl:value-of select="$mid"/>
<xsl:text>page</xsl:text><xsl:value-of select="$mid"/>
<xsl:text>fpage</xsl:text><xsl:value-of select="$mid"/>
<xsl:text>address</xsl:text><xsl:value-of select="$mid"/>
<xsl:text>abstract</xsl:text><xsl:value-of select="$mid"/>
<xsl:text>abstractor</xsl:text><xsl:value-of select="$mid"/>
<xsl:text>keywords</xsl:text><xsl:value-of select="$mid"/>
<xsl:text>source</xsl:text><xsl:value-of select="$mid"/>
<xsl:text>inserted</xsl:text><xsl:value-of select="$mid"/>
<xsl:text>oktoshow</xsl:text><xsl:value-of select="$mid"/>
<xsl:text>recordstatus</xsl:text>
<xsl:if test="@format='csv'"><xsl:text>"</xsl:text></xsl:if>
<xsl:text>&#10;</xsl:text>
<!-- imported text here -->
<xsl:for-each  select="*">
<!--tagname above depends on table name -->
<xsl:if test="../@format='csv'"><xsl:text>"</xsl:text></xsl:if>
<xsl:value-of select="recordid"/><xsl:value-of select="$mid"/>
<xsl:value-of select="normalize-space(authorkey)"/>
<xsl:value-of select="$mid"/>
<xsl:value-of select='normalize-space(author)'/><xsl:value-of select="$mid"/>
<xsl:value-of select="year"/><xsl:value-of select="$mid"/>
<xsl:value-of select='normalize-space(title)'/><xsl:value-of select="$mid"/>
<xsl:value-of select="normalize-space(serial)"/><xsl:value-of select="$mid"/>
<xsl:value-of select="volume"/><xsl:value-of select="$mid"/>
<xsl:value-of select="spage"/><xsl:value-of select="$mid"/>
<xsl:value-of select="fpage"/><xsl:value-of select="$mid"/>
<xsl:value-of select="normalize-space(address)"/><xsl:value-of select="$mid"/>
<xsl:value-of select="normalize-space(abstract)"/><xsl:value-of select="$mid"/>
<xsl:value-of select="abstractor"/><xsl:value-of select="$mid"/>
<xsl:value-of select="normalize-space(keywords)"/><xsl:value-of select="$mid"/>
<xsl:value-of select="normalize-space(source)"/><xsl:value-of select="$mid"/>
<xsl:value-of select="inserted"/><xsl:value-of select="$mid"/>
<xsl:value-of select="oktoshow"/><xsl:value-of select="$mid"/>
<xsl:value-of select="recordstatus"/>
<xsl:if test="../@format='csv'"><xsl:text>"</xsl:text></xsl:if>
<xsl:text>&#10;</xsl:text>
</xsl:for-each>
 </xsl:template>
  </xsl:stylesheet>	
  